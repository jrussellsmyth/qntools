#include <vector>
#include "qsyx.h"

namespace qsyx{
int calcCrc(std::vector<unsigned char>& msg) {
    int crc = 0xFFFF;
    for (unsigned char it : msg) {
        int temp = 0;
        int quick = 0;
        temp = (crc >> 8) ^ it; //xor 8 bit val with upper byte of crc (0x00HH ^ val) = 0x00XX
        crc <<= 8; // left shift crc now 0xLL00
        crc &= 0xFFFF;
        quick = ((temp ^ (temp >> 4)) & 0xFFFF); //0x00XX ^ 0x000X = 0x00XY
        crc ^= quick; // 0xLL00 ^ 0x00XY = 0xLLXY
        crc &= 0xFFFF; //effect of all this is to preserve the information in
        //LSB (LL) intact, while mixing the new data and the old MSB thoroughly
        quick <<= 5; //hash	(0x00XY << 5) = 0xNNN0	 (quick * 2 to the fifth)
        quick &= 0xFFFF;
        crc ^= quick; //hash
        crc &= 0xFFFF;
        quick <<= 7; //hash	(0xNNN0 << 7) = 0xN000
        quick &= 0xFFFF; //	 (quick * 2 to the seventh)
        crc ^= quick; //hash
        crc &= 0xFFFF;
    }
    return crc;
}

unsigned char msb(int val) {
    return (unsigned char) (val >> 8) & 0xFF;
}

unsigned char lsb(int val) {
    return (unsigned char) val & 0xFF;
}

std::vector<unsigned char> encode(std::vector<unsigned char> unencoded) {
    std::vector<unsigned char> rv;
    unsigned char hiBits = 0;
    int hiCount = 0;
    int padBytesNeeded = (7 - unencoded.size() % 7);
    for (unsigned char bite : unencoded) {
        hiBits |= (bite & 0x80);
        hiBits >>= 1;
        hiCount++;
        rv.push_back(bite & 0x7f);
        if (hiCount == 7) {
            rv.push_back(hiBits);
            hiCount = 0;
            hiBits = 0; // not really necessary, will be shifted 8 bits anyway.
        }
    }
    if(padBytesNeeded){
        rv.insert(rv.end(), padBytesNeeded, kPadByte);
        hiBits >>= padBytesNeeded;
        rv.push_back(hiBits);
    }
    return rv;
}

std::vector<unsigned char> generatePreamble(int type, int id) {
    std::vector<unsigned char> rv {msb(type), lsb(type), msb(id), lsb(id)};
    int crc = calcCrc(rv);
    rv.push_back(msb(crc));
    rv.push_back(lsb(crc));
    return rv;
}


}
