/* 
 * File:   QuNeoSyxStructs.h
 * Author: jrussell
 *
 * Created on December 6, 2012, 10:59 PM
 */

#ifndef QUNEOSYXSTRUCTS_H
#define	QUNEOSYXSTRUCTS_H
#include <cstdlib>
#include <vector>
#include <list>
#include <string>

    using namespace std;

    // trying to find
    // root["QuNeo Presets"][presetNum]["ComponentSettings"]["Pads"]["Pad0"]["enableGrid"]
    //***** helper arrays


    const unsigned char SYSEX_START = 0xF0;
    const unsigned char MFG_ID[]{ 0x00, 0x01, 0x5f, 0x7a };
    const unsigned char PRODUCT_ID = 0x1E;
    const unsigned char FORMAT = 0x00;
    const unsigned char SX_PACKET_START = 0x01;
    const unsigned char SYSEX_END = 0xF7;

    
    const int TYPE = 0x0002;
    const int ID_PRESET = 0x2220;
    const int ID_FIRMWARE = 0x1110;

    const unsigned char PRESET_START = 0xA1;
    //const int PRESET_LENGTH = 951;
    const int PRESET_LENGTH = 1279;
    const vector<unsigned char> qneo_sysex_header
    {
        // HEADER
        SYSEX_START, MFG_ID[0], MFG_ID[1], MFG_ID[2], MFG_ID[3], PRODUCT_ID, FORMAT
    };

    /**
     * Keys for presets within a QuNeo.json preset collection file.
     */
    const vector<string> preset_keys
    {
        "Preset 0", "Preset 1", "Preset 2", "Preset 3", "Preset 4", "Preset 5", "Preset 6", "Preset 7", "Preset 8", "Preset 9", "Preset 10", "Preset 11", "Preset 12", "Preset 13", "Preset 14", "Preset 15"
    };
    
    /**
     * Properties in the JSON file that are two byte (vs 1 byte) values. Used when coding SysEx from JSON.
     */
    const vector<string> two_byte_properties
    {
        "rB1outSpeed", "rB2outSpeed", "rB3outSpeed", "rB4outSpeed"
    };
    
    /**
     * Properties to retrieve from json for each pad, in sysex byte order.
     */
    const list<string> padProperties
    {
	"enableGrid", 
	"padChannel",
	"outDmNote", 
	"outDmNotePressMode",
	"outDmPress", 
	"outDmPressValue",
	"outDmVelocityValue", 
	"outDmXCC", 
	"outDmXYReturn",
	"outDmXReturn",
	"outDmYCC",
	"outDmYReturn",
	"padSensitivityPerPad", 


	"padChannel",
	"outGmNoteNW", 
	"outGmNotePressModeNW",
	"outGmPressNW", 
	"outGmPressValueNW",
	"outGmVelocityValueNW",


	"padChannel",
	"outGmNoteNE", 
	"outGmNotePressModeNE",
	"outGmPressNE", 
	"outGmPressValueNE",
	"outGmVelocityValueNE",

	
	"padChannel",
	"outGmNoteSE", 
	"outGmNotePressModeSE",
	"outGmPressSE", 
	"outGmPressValueSE",
	"outGmVelocityValueSE",		


	"padChannel",
	"outGmNoteSW", 
	"outGmNotePressModeSW",
	"outGmPressSW", 
	"outGmPressValueSW",
	"outGmVelocityValueSW"
    };
    /**
     * Properties to retrieve from json for each rotary, in sysex byte order.
     */
    const list<string> rotaryProperties
    {
	"rB1Channel",
	"rB1outLocation", 
	"rB1outNote",
	"rB1outNotePressMode",
	"rB1outPress", 
	"rB1outPressValue",
	"rB1outVelocityValue",
	"rB1outLocPassThruRange", 

	"rB1outDirection",
	"rB1outDirectionEnable",
	"rB1outSpeed",



	"rB2Channel",
	"rB2outLocation", 
	"rB2outNote",
	"rB2outNotePressMode",
	"rB2outPress", 
	"rB2outPressValue",
	"rB2outVelocityValue",
	"rB2outLocPassThruRange", 

	"rB2outDirection",
	"rB2outDirectionEnable",
	"rB2outSpeed",



	"rB3Channel",
	"rB3outLocation", 
	"rB3outNote",
	"rB3outNotePressMode",
	"rB3outPress", 
	"rB3outPressValue",
	"rB3outVelocityValue",
	"rB3outLocPassThruRange", 

	"rB3outDirection",
	"rB3outDirectionEnable",
	"rB3outSpeed",



	"rB4Channel",
	"rB4outLocation", 
	"rB4outNote",
	"rB4outNotePressMode",
	"rB4outPress", 
	"rB4outPressValue",
	"rB4outVelocityValue",
	"rB4outLocPassThruRange", 

	"rB4outDirection",
	"rB4outDirectionEnable",
	"rB4outSpeed"
    };

    /**
     * Properties to retrieve from json for the Long (cross) Slider, in sysex byte order.
     */
    const list<string> longSliderProperties
    {
	"lB1Channel",
	"lB1outLocation", 
	"lB1outNote",
	"lB1outNotePressMode",
	"lB1outPress", 
	"lB1outPressValue",
	"lB1outVelocityValue",
	"lB1outLocPassThruRange", 
  
	"lB1outWidth",  
	  
	"lB2Channel",
	"lB2outLocation", 
	"lB2outNote",
	"lB2outNotePressMode",
	"lB2outPress", 
	"lB2outPressValue",
	"lB2outVelocityValue",
	"lB2outLocPassThruRange",  

	"lB2outWidth", 
	
	"lB3Channel",
	"lB3outLocation", 
	"lB3outNote",
	"lB3outNotePressMode",
	"lB3outPress", 
	"lB3outPressValue",
	"lB3outVelocityValue",
	"lB3outLocPassThruRange",    

	"lB3outWidth", 
	
	"lB4Channel",
	"lB4outLocation", 
	"lB4outNote",
	"lB4outNotePressMode",
	"lB4outPress", 
	"lB4outPressValue",
	"lB4outVelocityValue",
	"lB4outLocPassThruRange",  

	"lB4outWidth"
    };

    /**
     * Properties to retrieve from json for each horizotal slider, in sysex byte order.
     */
    const list<string> horizontalSliderProperties
    {
	"hB1Channel",
	"hB1outLocation", 
	"hB1outNote",
	"hB1outNotePressMode",
	"hB1outPress", 
	"hB1outPressValue",
	"hB1outVelocityValue",
	"hB1outLocPassThruRange", 
 
	
	"hB2Channel",
	"hB2outLocation", 
	"hB2outNote",
	"hB2outNotePressMode",
	"hB2outPress", 
	"hB2outPressValue",
	"hB2outVelocityValue",
	"hB2outLocPassThruRange", 

	
	"hB3Channel",
	"hB3outLocation", 
	"hB3outNote",
	"hB3outNotePressMode",
	"hB3outPress", 
	"hB3outPressValue",
	"hB3outVelocityValue",
	"hB3outLocPassThruRange", 

	
	"hB4Channel",
	"hB4outLocation", 
	"hB4outNote",
	"hB4outNotePressMode",
	"hB4outPress", 
	"hB4outPressValue",
	"hB4outVelocityValue",
	"hB4outLocPassThruRange"
    };

    /**
     * Properties to retrieve from json for each vertical slider, in sysex byte order.
     */
    const list<string> verticalSliderProperties
    {
    	"vB1Channel",
	"vB1outLocation", 
	"vB1outNote",
	"vB1outNotePressMode",
	"vB1outPress", 
	"vB1outPressValue",
	"vB1outVelocityValue",
	"vB1outLocPassThruRange", 
 
	
	"vB2Channel",
	"vB2outLocation", 
	"vB2outNote",
	"vB2outNotePressMode",
	"vB2outPress", 
	"vB2outPressValue",
	"vB2outVelocityValue",
	"vB2outLocPassThruRange", 

	
	"vB3Channel",
	"vB3outLocation", 
	"vB3outNote",
	"vB3outNotePressMode",
	"vB3outPress", 
	"vB3outPressValue",
	"vB3outVelocityValue",
	"vB3outLocPassThruRange", 

	
	"vB4Channel",
	"vB4outLocation", 
	"vB4outNote",
	"vB4outNotePressMode",
	"vB4outPress", 
	"vB4outPressValue",
	"vB4outVelocityValue",
	"vB4outLocPassThruRange"
    };
    /**
     * Properties to retrieve from json for each l/r switch, in sysex byte order.
     */

    const list<string> lrSwitchProperties
    {
	"leftrightEnableSwitch", 
	
	"leftrightChannel",
	"leftrightLOutNote", 
	"leftrightLOutNotePressMode",
	"leftrightLOutPress", 
	"leftrightLOutPressValue",
	"leftrightLOutVelocityValue",

	 	 
	"leftrightChannel",
	"leftrightROutNote", 
	"leftrightROutNotePressMode",
	"leftrightROutPress", 
	"leftrightROutPressValue",
	"leftrightROutVelocityValue"
    };
    /**
     * Properties to retrieve from json for each rotary, in sysex byte order.
     */
    const list<string> udSwitchProperties
    {
 	"updownEnableSwitch", 
	"updownBankControl", 
	
	"updownChannel", 
	"updownUOutNote", 
	"updownUOutNotePressMode",
	"updownUOutPress", 
	"updownUOutPressValue",
	"updownUOutVelocityValue", 

	 
	"updownChannel", 
	"updownDOutNote", 
	"updownDOutNotePressMode",
	"updownDOutPress", 
	"updownDOutPressValue",
	"updownDOutVelocityValue"
    };
    /**
     * Properties to retrieve from json for the Rhombus switch, in sysex byte order.
     */
    const list<string> rhombusSwitchProperties
    {
	"rhombusEnableSwitch", 
	"rhombusBankControl", 
	"rhombusInNoteG",
	"rhombusInNoteR",
	
	"rhombusChannel", 
	"rhombusOutNote",
	"rhombusOutNotePressMode", 
	"rhombusOutPress",
	"rhombusOutPressValue",
	"rhombusOutVelocityValue"
    };

    /**
     * Properties to retrieve from json for each transportButton, in sysex byte order.
     */
    const list<string> tbutt_str
    {
   	"transportChannel", 
	"transportOutNote", 
	"transportOutNotePressMode",
	"transportOutPress",
	"transportOutPressValue",
	"transportOutVelocityValue"
    };

    const list<string> mswitch_str
    {
 	"modeOutVelocityValue", 
	"modeEnableSwitch",  
	"modeChannel", 
	"modeOutNote", 
	"modeOutPress"
    };

    vector<pair<string, vector<pair<string, list<string >> >> > structureMap
    //	map<string, map<string, vector < string >> > structureMap
    {
        {
            "Pads",
            {
                {"Pad0", padProperties},
                {"Pad1", padProperties},
                {"Pad2", padProperties},
                {"Pad3", padProperties},
                {"Pad4", padProperties},
                {"Pad5", padProperties},
                {"Pad6", padProperties},
                {"Pad7", padProperties},
                {"Pad8", padProperties},
                {"Pad9", padProperties},
                {"Pad10", padProperties},
                {"Pad11", padProperties},
                {"Pad12", padProperties},
                {"Pad13", padProperties},
                {"Pad14", padProperties},
                {"Pad15", padProperties},
            }
        },
        {
            "Rotaries",
            {
                {"Rotary0", rotaryProperties},
                {"Rotary1", rotaryProperties}
            }
        }
        ,
        {
            "LongSliders",
            {
                {"LongSlider0", longSliderProperties}
            }
        }
        ,
        {
            "HSliders",
            {
                {"HSlider0", horizontalSliderProperties},
                {"HSlider1", horizontalSliderProperties},
                {"HSlider2", horizontalSliderProperties},
                {"HSlider3", horizontalSliderProperties}
            }
        }
        ,
        {
            "VSliders",
            {
                {"VSlider0", verticalSliderProperties},
                {"VSlider1", verticalSliderProperties},
                {"VSlider2", verticalSliderProperties},
                {"VSlider3", verticalSliderProperties}
            }
        }
        ,
        {
            "LeftRightButtons",
            {
                {"LeftRightButton0", lrSwitchProperties},
                {"LeftRightButton1", lrSwitchProperties},
                {"LeftRightButton2", lrSwitchProperties},
                {"LeftRightButton3", lrSwitchProperties}
            }
        }
        ,
        {
            "UpDownButtons",
            {
                {"UpDownButton0", udSwitchProperties},
                {"UpDownButton1", udSwitchProperties}
            }
        }
        ,
        {
            "RhombusButtons",
            {
                {"RhombusButton0", rhombusSwitchProperties}
            }
        }
        ,
        {
            "TransportButtons",
            {
                {"TransportButton0", tbutt_str},
                {"TransportButton1", tbutt_str},
                {"TransportButton2", tbutt_str}
            }
        },
        {
            "ModeButtons",
            {
                {"ModeButton0", mswitch_str}
            }
        }
    };


    const vector<vector<string >> kGlobalPresetSettings
    {
        
        {"Pads","padBankChangeMode"},
        {"HSliders","hSliderInChannel"},
        {"LeftRightButtons","leftrightInChannel"},
        {"LongSliders","lSliderInChannel"},
        {"Pads","padDrumInChannel"},
        {"Pads","padGridDiscreteInChannel"},
        {"Pads","padGridDualInChannel"},
        {"RhombusButtons","rhombusInChannel"},
        {"Rotaries","rotaryInChannel"},
        {"TransportButtons","transportInChannel"},
        {"UpDownButtons","updownInChannel"},
        {"VSliders","vSliderInChannel"},
        {"Pads","bank1TransposeInterval"}, 
        {"Pads","bank2TransposeInterval"}, 
        {"Pads","bank3TransposeInterval"}, 
        {"Pads","bank4TransposeInterval"}, 
        {"VSliders", "vSliderOffThreshold"},
        {"VSliders", "vSliderOnThreshold"},
        {"UpDownButtons", "updownOffThreshold"},
        {"UpDownButtons", "updownOnThreshold"},
        {"TransportButtons", "transportOffThreshold"},
        {"TransportButtons", "transportOnThreshold"},
        {"Rotaries", "rotaryOffThreshold"},
        {"Rotaries", "rotaryOnThreshold"},
        {"RhombusButtons", "rhombusOffThreshold"},
        {"RhombusButtons", "rhombusOnThreshold"},
        {"ModeButtons", "modeOffThreshold"},
        {"ModeButtons", "modeOnThreshold"},
        {"LongSliders", "lSliderOffThreshold"},
        {"LongSliders", "lSliderOnThreshold"},
        {"LeftRightButtons", "leftrightOffThreshold"},
        {"LeftRightButtons", "leftrightOnThreshold"},
        {"HSliders", "hSliderOffThreshold"},
        {"HSliders", "hSliderOnThreshold"},
        {"Pads", "padOffset"},
        {"Pads", "padOffThreshold"},
        {"Pads", "padOnThreshold"},
        {"Pads", "cornerIsolation"},
        {"Pads", "padSensitivity"},
        {"Rotaries", "rotarySensitivity"},
        {"LongSliders", "lSliderSensitivity"},
        {"HSliders", "hSliderSensitivity"},
        {"VSliders", "vSliderSensitivity"},
        {"LeftRightButtons", "leftrightSensitivity"},
        {"UpDownButtons", "updownSensitivity"},
        {"RhombusButtons", "rhombusSensitivity"},
        {"TransportButtons", "transportSensitivity"},
        {"ModeButtons", "modeSensitivity"},
        {"Pads", "localLEDControl"},
        {"Rotaries", "rotaryLocalLEDControl"},
        {"LongSliders", "lSliderLocalLEDControl"},
        {"HSliders", "hSliderLocalLEDControl"},
        {"VSliders", "vSliderLocalLEDControl"},
        {"LeftRightButtons", "leftrightLocalLEDControl"},
        {"UpDownButtons", "updownLocalLEDControl"},
        {"RhombusButtons", "rhombusLocalLEDControl"},
        {"TransportButtons", "transportLocalLEDControl"},
        {"ModeButtons", "modeLocalLEDControl"}
    };
#endif	/* QUNEOSYXSTRUCTS_H */

