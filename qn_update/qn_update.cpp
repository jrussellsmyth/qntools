/* 
 * Copyright 2012 J. Russell Smyth
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Created on December 4, 2012, 3:18 PM
 */
// possibilities to try Boost.PropertyTree 
#include <cstdlib>
#include <cstring> // for strtok
#include <iostream>
#include <fstream>
#include <iterator>
#include <array>
#include <iomanip>
#include <algorithm>
#include <getopt.h>
#include "jsoncpp/json/json.h"
#include "RtMidi.h"
#include "QuNeoSyxStructs.h"
#include "qsyx.h"

// Platform-dependent sleep routines.
#if defined(__WINDOWS_MM__)
#include <windows.h>
#define SLEEP( milliseconds ) Sleep( (DWORD) milliseconds )
#else // Unix variants
#include <unistd.h>
#define SLEEP( milliseconds ) usleep( (unsigned long) (milliseconds * 1000.0) )
#endif

using namespace std;



void version();
void useage();
vector<unsigned char> presetJsonToSysex(int index, int finalIndex, Json::Value preset);
void appendVector(vector<unsigned char> &target, vector<unsigned char> &source);
void sendPresetData(int presetId, Json::Value presetData, int portNo);
void sendPresetChange(int presetId, int portNo);
void sendSysex(vector<unsigned char> message, int port);
void listPorts();
int getDefaultDevice();

/*
 * 
 */
int main(int argc, char** argv) {

    string inputFile;
    string outputFile;
    int newPresetNum = -1;
    bool send = false;
    bool isQPreset = false;
    Json::Value presetFileJson;
    Json::Reader reader;
    int firstPreset = 0;
    int lastPreset = 0;
    int device = -1;

    if (!argv[1]) useage();

    const struct option longopts[]{
        {"list", no_argument, 0, 'l'},
        { "device", required_argument, 0, 'd'},
        { "help", no_argument, 0, 'h'},
        { "input", required_argument, 0, 'i'},
        { "presets", required_argument, 0, 'n'},
        { "preset", required_argument, 0, 'n'},
        { "output", required_argument, 0, 'o'},
        { "change-preset", required_argument, 0, 'c'},
        { "version", no_argument, 0, 'v'},
        { "send", no_argument, 0, 'x'}
    };

    int optc;
    int option_index;
    while ((optc = getopt_long(argc, argv, "d:hi:n:o:c:vx", longopts, &option_index)) != EOF) {
        switch (optc) {
            case 'i':
                inputFile = optarg;
                break;
            case 'n':
                char * pch;
                pch = strtok(optarg, ".,:-");
                firstPreset = atoi(pch);
                pch = strtok(NULL, ".,:-");
                if (pch != NULL) {
                    lastPreset = atoi(pch);
                }
                if (firstPreset > 16) firstPreset = 16;
                if (lastPreset > 16) lastPreset = 16;
                if (lastPreset < firstPreset) lastPreset = firstPreset;
                break;
            case 'o':
                outputFile = optarg;
                break;
            case 'c':
                newPresetNum = atoi(optarg);
                break;
            case 'x':
                send = true;
                break;
            case 'd':
                device = atoi(optarg);
                break;
            case 'l':
                listPorts();
            case 'v':
                version();
            case 'h':
            default:
                useage();
                exit(2);
        }
    }

    if (device < 0) {
        device = getDefaultDevice();
    }
    if (inputFile.length() > 0) {
        ifstream inputStream(inputFile, ios::in | ios::binary);
        bool parsingSuccessful = reader.parse(inputStream, presetFileJson);
        inputStream.close();

        if (!parsingSuccessful) {
            // report to the user the failure and their locations in the document.
            std::cout << "Failed to parse preset file\n" << reader.getFormattedErrorMessages();
            exit(1);
        }

        if (presetFileJson.isMember("ComponentSettings")) {
            isQPreset = true;
        } else if (presetFileJson.isMember("QuNeo Presets")) {
            isQPreset = false;
        } else {
            cout << "file \"" << inputFile << "\" is not a valid preset file." << endl;
            exit(1);
        }

        // sending a range of preset # is error with .quneopreset file
        if (isQPreset && firstPreset != lastPreset) {
            cout << ".qpreset file requires single target preset number (-n|--presets|--preset)" << endl;
            exit(1);
        }

        // write presets 
        if (isQPreset) { // single preset from a .quneopreset file
            cout << "Transmitting single preset to " << preset_keys[firstPreset] << endl;
            sendPresetData(firstPreset, presetFileJson["ComponentSettings"], device);
        } else {
            for (int presetIndex = firstPreset; presetIndex <= lastPreset; presetIndex++) {
                cout << "Transmitting " << preset_keys[presetIndex] << endl;
                sendPresetData(presetIndex, presetFileJson["QuNeo Presets"][preset_keys[presetIndex]]["ComponentSettings"], device);
                SLEEP(200); // prevent midi buffer overflows. Hack but best available currently.
            }
        }
    }

    if (newPresetNum > -1) {
        sendPresetChange(newPresetNum, device);
    }

    return 0;
}

/**
 * Convert provided JSON data to a sysex message and transmit to QuNeo
 * @param presetId 0 indexed preset number to update with provided data
 * @param presetData
 */
void sendPresetData(int presetId, Json::Value presetData, int portNo) {
    vector<unsigned char> message(qneo_sysex_header);
    message.push_back(SX_PACKET_START);
    vector<unsigned char> preamble = qsyx::encode(qsyx::generatePreamble(TYPE, ID_PRESET));
    appendVector(message, preamble);
    vector<unsigned char> presetSysex = qsyx::encode(presetJsonToSysex(presetId, presetId, presetData));
    appendVector(message, presetSysex);
    message.push_back(SYSEX_END);
    sendSysex(message, portNo);
}

void sendPresetChange(int presetId, int portNo) {
    vector<unsigned char> preset_sysex(qneo_sysex_header);
    // write sx_packet start
    preset_sysex.push_back(SX_PACKET_START);
    // write preamble block
    int presetSelector = 0x3000 | (presetId & 0xFF);
    vector<unsigned char> body = qsyx::encode(qsyx::generatePreamble(TYPE, presetSelector));
    appendVector(preset_sysex, body);
    preset_sysex.push_back(SYSEX_END);
    sendSysex(preset_sysex, portNo);
}

vector<unsigned char> presetJsonToSysex(int index, int finalIndex, Json::Value preset) {
    vector<unsigned char> result;
    unsigned char sumbyte = 0;
    result.reserve(PRESET_LENGTH);
    // preset header
    result.push_back(PRESET_START);
    result.push_back(index);
    result.push_back(qsyx::msb(PRESET_LENGTH));
    result.push_back(qsyx::lsb(PRESET_LENGTH));

    for (pair < string, vector < pair < string, list < string >> >> controllGroup : structureMap) {
        for (pair < string, list < string >> controll : controllGroup.second) {
            for (string property : controll.second) {
                if (std::find(two_byte_properties.begin(), two_byte_properties.end(), property) != two_byte_properties.end()) {
                    /* two byte */
                    int value = preset[controllGroup.first][controll.first][property].asInt();
                    unsigned char lsb = qsyx::lsb(preset[controllGroup.first][controll.first][property].asInt());
                    unsigned char msb = qsyx::msb(preset[controllGroup.first][controll.first][property].asInt());
                    result.push_back(msb);
                    sumbyte += msb;
                    result.push_back(lsb);
                    sumbyte += lsb;
                } else {
                    /* not two byte */
                    unsigned char byteValue = (unsigned char) preset[controllGroup.first][controll.first][property].asInt();
                    result.push_back(byteValue);
                    sumbyte += byteValue;
                }
            }
        }
    }
    // globals
    for (vector<string > key : kGlobalPresetSettings) {
        unsigned char byteValue = (unsigned char) preset[key[0]][key[1]].asInt();
        result.push_back(byteValue);
        sumbyte += byteValue;

    }
    // velocity table
    for (int vti = 0; vti < 128; vti++) {
        unsigned char byteValue = (unsigned char) preset["Pads"]["padVelocityTable"][vti].asInt();
        //		cout << "velocity table entry[" <<vti<<"] = 0x" << setw(2) << setfill('0') << hex << (int) byteValue << endl;
        result.push_back(byteValue);
        sumbyte += byteValue;
    }
    // preset number
    result.push_back(index);
    // endNum - 1 (last index?)
    result.push_back(finalIndex);
    // sumbyte
    result.push_back(sumbyte);
    return result;
}

void sendSysex(vector<unsigned char> message, int portNo) {
    RtMidiOut midiOut(RtMidi::LINUX_ALSA);
    try {
        midiOut.openPort(portNo);
        midiOut.sendMessage(&message);
    } catch (RtError &error) {
        cout << "An error occurred sending messge to QuNeo:" << endl;
        error.printMessage();
    }
    midiOut.closePort();
    return;
}

vector<pair<int, string >> enumeratePorts() {
    vector < pair<int, string >> portSpecs;
    RtMidiOut midiOut(RtMidi::LINUX_ALSA);
    int numPorts = midiOut.getPortCount();
    for (int portId = 0; portId < numPorts; portId++) {
        portSpecs.push_back(pair<int, string>(portId, midiOut.getPortName(portId)));
    }
    return portSpecs;
}

void listPorts() {
    for (pair<int, string> portSpec : enumeratePorts()) {
        cout << "  #" << portSpec.first << " : " << portSpec.second << endl;
    }

}

int getDefaultDevice() {
    int rv = 0;
    for (pair<int, string> portSpec : enumeratePorts()) {
        string portName = portSpec.second;
        std::transform(portName.begin(), portName.end(), portName.begin(), ::toupper);
        if (string::npos != portName.find("QUNEO")) {
            rv = portSpec.first;
            break;
        }
    }
    return rv;
}
// should be template for other vector types

void appendVector(vector<unsigned char> &target, vector<unsigned char> &source) {
    target.reserve(target.size() + source.size());
    target.insert(target.end(), source.begin(), source.end());
}

void version() {
    /*
      cerr << progname << " " << VERSION << " for " << QPUPDATE_OS << endl;
          cerr << "Built " << __DATE__ << "-" << __TIME__ << "with" << COMPILER
     */
    exit(2);
}

void useage() {
    cout << "qn_update version UNKNOWN" /*VERSION*/ << endl;
    cout << " Copyright (C) 2013 J Russell Smyth. LICENSE TO BE DETERMINED." << endl;
    cout << " NO WARRANTEE. This program is protected by the" << endl;
    cout << " GNU General Public License.\n\n" << endl;
    cout << "Usage: qn_update [options] [-i <filename>] [-o <filename>]" << endl;
    cout << "  -i --input              input file (presets/QuNeo.json)" << endl;
    cout << "  -o --output             output file to store .syx message (none/dont write)" << endl;
    cout << "  -n --presets,--preset   preset to write single preset file to or " << endl;
    cout << "        which preset(s) to include from a set (QuNeo.json) file" << endl;
    cout << "        single preset number or colon or dash seperated range. (0)" << endl;
    cout << "  -c --change-preset      preset to change QuNeo to after update (0)" << endl;
    cout << "  -d --device             device name to write sysex message to (" << "found quneo device" << ")" << endl;
    cout << "  -x --send               send preset(s) to device and set current preset" << endl;
    cout << "  -v --version            print program version and exit" << endl;
    cout << "  -C --copyright          print copyright info and exit" << endl;
    cout << "  -h --help               print this help and exit" << endl;
    exit(2);
}

