/**
 * Useful functions for preparing sysex messages for QuNeo (and possibly other KMI products)
 */
#ifndef QSYX_H
#define QSYX_H
#include <vector>
namespace qsyx {
const unsigned char kPadByte = 0x00;
/**
 * Calculate crc for provided message.
 * @param msg the message
 * @return the crc value
 */
int calcCrc(std::vector<unsigned char>& msg);
/**
 * Extract most significant byte of an (two byte) integer.
 * @param val to extract from
 * @return the msb
 */
unsigned char msb(int val);
/**
 * Extract least significant byte of an (two byte) integer.
 * @param val to extract from
 * @return the lsb
 */
unsigned char lsb(int val);
/**
 * Generate the preamble block for a QuNeo message block.
 * @param type
 * @param id
 * @return 
 */
std::vector<unsigned char> generatePreamble(int type, int id);
/**
 * Encode series of 8 bit values as 7 bit with high bits written as extra byte.
 * Message is padded to multiple of 7 values to allow for correct 8th byte
 * calculation
 * @param unencoded vector
 * @return encoded vector
 */
std::vector<unsigned char> encode(std::vector<unsigned char> unencoded);

}
#endif // QSYX_H
