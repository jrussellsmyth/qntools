/* 
 * File:   quneoJson.h
 * Author: jrussell
 *
 * Created on December 30, 2012, 4:37 AM
 */

#ifndef QUNEOJSON_H
#define	QUNEOJSON_H
#include <vector>
#include <string>

using namespace std;
const vector<string> preset_properties{
    "ComponentSettings",
    "presetName", // "Ableton Live Drum Rack",
      "revisionNumber" //: 576
};
const vector<string> component_settings_properties{
    "HSliders",
    "LeftRightButtons",
    "LongSliders",
    "ModeButtons",
    "Pads",
    "RhombusButtons",
    "Rotaries",
    "TransportButtons",
    "UpDownButtons",
    "VSliders"
};
const vector<string> pad_properties
{
    "enableGrid",
    "inDmNoteG",
    "inDmNoteR",
    "inGmNoteGNE",
    "inGmNoteGNW",
    "inGmNoteGSE",
    "inGmNoteGSW",
    "inGmNoteRNE",
    "inGmNoteRNW",
    "inGmNoteRSE",
    "inGmNoteRSW",
    "outDmNote",
    "outDmNotePressMode",
    "outDmPress",
    "outDmPressValue",
    "outDmVelocityValue",
    "outDmXCC",
    "outDmXReturn",
    "outDmXYReturn",
    "outDmYCC",
    "outDmYReturn",
    "outGmNoteNE",
    "outGmNoteNW",
    "outGmNotePressModeNE",
    "outGmNotePressModeNW",
    "outGmNotePressModeSE",
    "outGmNotePressModeSW",
    "outGmNoteSE",
    "outGmNoteSW",
    "outGmPressNE",
    "outGmPressNW",
    "outGmPressSE",
    "outGmPressSW",
    "outGmPressValueNE",
    "outGmPressValueNW",
    "outGmPressValueSE",
    "outGmPressValueSW",
    "outGmVelocityValueNE",
    "outGmVelocityValueNW",
    "outGmVelocityValueSE",
    "outGmVelocityValueSW",
    "padChannel",
    "padSensitivityPerPad",
    "padSensitivityPerPadUI"
};
const vector<string> pads_properties
{
    "pad0",
    "pad1",
    "pad2",
    "pad3",
    "pad4",
    "pad5",
    "pad6",
    "pad7",
    "pad8",
    "pad9",
    "pad10",
    "pad11",
    "pad12",
    "pad13",
    "pad14",
    "pad15",
    "bank1TransposeInterval",
    "bank2TransposeInterval",
    "bank3TransposeInterval",
    "bank4TransposeInterval",
    "cornerIsolation",
    "localLEDControl",
    "padBankChangeMode",
    "padDrumInChannel",
    "padGridDiscreteInChannel",
    "padGridDualInChannel",
    "padOffThreshold",
    "padOffset",
    "padOnThreshold",
    "padSensitivity",
    "padVelocityTable"
};


#endif	/* QUNEOJSON_H */

