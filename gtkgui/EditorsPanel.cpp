/* 
 * File:   EditorsPanel.cpp
 * Author: jrussell
 * 
 * Created on January 1, 2013, 2:08 AM
 */

#include "EditorsPanel.h"
#include <iostream>

EditorsPanel::EditorsPanel(){}
EditorsPanel::EditorsPanel(BaseObjectType* cobject, const Glib::RefPtr<Gtk::Builder>& refGlade) :
Gtk::Notebook(cobject), builder(refGlade){
}

void EditorsPanel::on_sensor_selected(std::string collection, int index){
    std::cout << "called on sensor selected";
    if(collection == "TransportButtons") {
        std::cout << "setting transport buttons" << std::endl;
        set_current_page(0);
    } else if (collection == "LeftRightButtons") {
        std::cout << "setting lr button" << std::endl;
        set_current_page(1);
    } else if (collection == "Pads") {
        std::cout << "setting pads" << std::endl;
        set_current_page(6);
    } else {
        std::cout << "ERROR: Sensor Type Unknown" << std::endl;
    }
}

