#include "PresetEditorMainWindow.h"
#include <gtkmm/application.h>
#include <gtkmm/builder.h>

int main(int argc, char *argv[]) {
    Glib::RefPtr<Gtk::Application> app = Gtk::Application::create(argc, argv, "gtkmmTest");
    Glib::RefPtr<Gtk::Builder> builder = Gtk::Builder::create_from_file("padEditor.glade");
    PresetEditorMainWindow *mainWin = nullptr;
    builder->get_widget_derived("testWindow", mainWin);
    //Shows the window and returns when it is closed.
    return app->run(*mainWin);
}