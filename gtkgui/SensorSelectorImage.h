/* 
 * File:   SensorSelectorImage.h
 * Author: jrussell
 *
 * Created on January 1, 2013, 12:32 PM
 */

#ifndef SENSORSELECTORIMAGE_H
#define	SENSORSELECTORIMAGE_H

#include <gtkmm/eventbox.h>
#include <gtkmm/builder.h>
#include "SensorSelector.h"
#include <vector>
#include <string>
#include <utility>
#include "imagemap.h"

using namespace std;
// vector<pair<imagemap::Box, pair<string, string>>> const interceptList {
//        
//    };

class SensorSelectorImage : public Gtk::EventBox, public SensorSelector{
   
    static vector<pair<imagemap::Box, pair<string, int>>> const interceptList;
    Glib::RefPtr<Gtk::Builder> builder;
    
        
public:
    SensorSelectorImage(BaseObjectType* cobject, const Glib::RefPtr<Gtk::Builder>& refGlade);
    SensorSelectorImage();
    SensorSelectorImage(const SensorSelectorImage& orig);
    virtual ~SensorSelectorImage();
    bool on_button_press_event(GdkEventButton* event);
private:

};

#endif	/* SENSORSELECTORIMAGE_H */

