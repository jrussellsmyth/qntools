/* 
 * File:   SensorSelector.h
 * Author: jrussell
 *
 * Created on January 1, 2013, 11:13 PM
 */

#ifndef SENSORSELECTOR_H
#define	SENSORSELECTOR_H

#include <string>
using namespace std;

class SensorSelector {
public:
    SensorSelector() {};
    virtual ~SensorSelector() {};
    sigc::signal<void, string, int> signal_selected() { return signalSelected; }
protected:
    sigc::signal<void, string, int> signalSelected; 
private:

};

#endif	/* SENSORSELECTOR_H */

