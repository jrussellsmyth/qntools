/* 
 * File:   TestWindow.cpp
 * Author: jrussell
 * 
 * Created on December 31, 2012, 12:01 AM
 */

#include "PresetEditorMainWindow.h"
#include <string>
#include <gtkmm-3.0/gtkmm/widget.h>
#include <glibmm-2.4/glibmm/signalproxy.h>

using namespace std;

PresetEditorMainWindow::PresetEditorMainWindow(BaseObjectType* cobject, const Glib::RefPtr<Gtk::Builder>& refGlade) :
Gtk::Window(cobject), builder(refGlade) {
    builder->get_widget_derived("sensorSelector", sensorSelector);
    builder->get_widget_derived("editorsStack", editorsStack);
    sensorSelector->signal_selected().connect(sigc::mem_fun(editorsStack, &EditorsPanel::on_sensor_selected));
}