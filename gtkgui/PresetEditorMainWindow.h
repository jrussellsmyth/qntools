/* 
 * File:   TestWindow.h
 * Author: jrussell
 *
 * Created on December 31, 2012, 12:01 AM
 */

#ifndef PRESETEDITORWINDOW_H
#define	PRESETEDITORWINDOW_H

#include <gtkmm/window.h>
#include <gtkmm/builder.h>
#include "PadFrame.h"
#include "SensorSelectorImage.h"
#include "EditorsPanel.h"

class PresetEditorMainWindow : public Gtk::Window {
protected:
    Glib::RefPtr<Gtk::Builder> builder;
    SensorSelectorImage * sensorSelector;
    EditorsPanel * editorsStack;
 
public:
    //    TestWindow();
    //    TestWindow(const TestWindow& orig);
    PresetEditorMainWindow(BaseObjectType* cobject, const Glib::RefPtr<Gtk::Builder>& refGlade);
//    virtual ~TestWindow();

protected:
    
private:

};

#endif	/* TESTWINDOW_H */

