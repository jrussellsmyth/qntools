/* 
 * File:   SensorSelectorImage.cpp
 * Author: jrussell
 * 
 * Created on January 1, 2013, 12:32 PM
 */

#include <iostream>

#include "SensorSelectorImage.h"

using namespace std;

vector<pair<imagemap::Box, pair<string, int>>> const SensorSelectorImage::interceptList {
// pads
    {{267, 365, 362, 460},{"Pads", 0}}, 
    {{378, 476, 362, 460},{"Pads", 1}}, 
    {{489, 587, 362, 460},{"Pads", 2}},
    {{601 ,699, 362, 460},{"Pads", 3}},
    {{267, 365, 248, 346},{"Pads", 4}},
    {{378, 476, 248, 346},{"Pads", 5}},
    {{489, 587, 248, 346},{"Pads", 6}},
    {{601 ,699, 248, 346},{"Pads", 7}},
    {{267, 365, 137, 236},{"Pads", 8}},
    {{378, 476, 137, 236},{"Pads", 9}},
    {{489, 587, 137, 236},{"Pads", 10}},
    {{601 ,699, 137, 236},{"Pads", 11}},
    {{267, 365, 25, 122},{"Pads", 12}},
    {{378, 476, 25, 122},{"Pads", 13}},
    {{489, 587, 25, 122},{"Pads", 14}},
    {{601 ,699, 25, 122},{"Pads", 15}},
//TransportButtons
    {{88, 131, 14, 57},{"TransportButtons", 0}}, 
    {{149, 188, 15, 55},{"TransportButtons", 1}} ,
    {{208, 247, 15 ,55},{"TransportButtons", 2}}, 
//LeftRightButtons
    {{15, 73, 72, 102},{"LeftRightButtons", 0}},
    {{15, 73, 125, 156},{"LeftRightButtons", 1}},
    {{15, 73, 179, 211},{"LeftRightButtons", 2}},
    {{15, 73, 235, 268},{"LeftRightButtons", 3}}    
        
    
    
    
};

SensorSelectorImage::SensorSelectorImage(BaseObjectType* cobject, const Glib::RefPtr<Gtk::Builder>& refGlade) :
Gtk::EventBox(cobject), builder(refGlade) {
}
SensorSelectorImage::SensorSelectorImage() {
}

SensorSelectorImage::SensorSelectorImage(const SensorSelectorImage& orig) {
}

SensorSelectorImage::~SensorSelectorImage() {
}

bool SensorSelectorImage::on_button_press_event(GdkEventButton* event) {
    //cout << "button" << event->button << " X:" << event->x << " Y:" << event->y << endl;
    for (pair<imagemap::Box, pair<string, int>> intercept : interceptList){
        if(intercept.first.contains({event->x, event->y})) {
            cout << "found intercept " << intercept.second.first << "-" <<  intercept.second.second << endl;;
            signalSelected.emit(intercept.second.first, intercept.second.second);
            
        }
    }
}
