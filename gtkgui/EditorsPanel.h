/* 
 * File:   EditorsPanel.h
 * Author: jrussell
 *
 * Created on January 1, 2013, 2:08 AM
 */

#ifndef EDITORSPANEL_H
#define	EDITORSPANEL_H

#include <gtkmm/notebook.h>
#include <gtkmm/builder.h>
#include <string>

class EditorsPanel : public Gtk::Notebook {
        Glib::RefPtr<Gtk::Builder> builder;
public:
    
    EditorsPanel();
    EditorsPanel(BaseObjectType* cobject, const Glib::RefPtr<Gtk::Builder>& refGlade);
    void on_sensor_selected(std::string collection, int index);
private:

};

#endif	/* EDITORSPANEL_H */

