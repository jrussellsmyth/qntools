/* 
 * File:   imagemap.h
 * Author: jrussell
 *
 * Created on January 2, 2013, 12:25 AM
 */

#ifndef IMAGEMAP_H
#define	IMAGEMAP_H

namespace imagemap{
struct Point {
    Point(double dx, double dy){ x = (int)dx; y = (int)dy; }
    int x;
    int y;
};
class Box {
    int left, right, top, bottom;
public:
    Box(int left, int right, int top, int bottom):left(left),right(right), top(top), bottom(bottom) {}
    bool contains(Point p) {
        if(left < p.x && p.x < right && top < p.y && p.y < bottom){
            return true;
        } else {
            return false;
        }
    }
};

}

#endif	/* IMAGEMAP_H */

