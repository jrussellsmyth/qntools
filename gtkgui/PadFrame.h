/* 
 * File:   PadFrame.h
 * Author: jrussell
 *
 * Created on December 31, 2012, 2:03 AM
 */

#ifndef PADFRAME_H
#define	PADFRAME_H
#include <gtkmm.h>

class PadFrame : public Gtk::Frame {
protected:
    Glib::RefPtr<Gtk::Builder> builder;
    
    Glib::RefPtr<Gtk::Adjustment> pressureAdjustment;
    Gtk::SpinButton *noteSpin;
    Gtk::SpinButton *velocitySpin;
public:
    PadFrame(BaseObjectType* cobject, const Glib::RefPtr<Gtk::Builder>& refGlade);
private:

};

#endif	/* PADFRAME_H */

