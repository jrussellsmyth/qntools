# qnTools - Open Source Tools to make QuNeo more usable on all platforms
The qnTools project aims to provide a set of tools to make the [KMI QuNeo](http://www.keithmcmillen.com/QuNeo) usable on all platforms, particularly Linux.

These tools will include a preset editor and tools to update the QuNeo's presets and firmware from all supported platforms.

We are also providing (unofficial at the moment) documentation of the QuNeo SysEx messages and editor JSON format in our [Wiki](https://bitbucket.org/jrussellsmyth/qntools/wiki/Home)

We are in the early stages of development and are open to all suggestions and/or input. Please try our tools and feel free to enter suggestions or bug reports into our [issue tracking](https://bitbucket.org/jrussellsmyth/qntools/issues) system on Bitbucket.

## qn_update - command line preset update tool
* currently supports firmware v1.2.x and json preset files from the KMI QuNeo preset editor v1.2.x
 ** may eventually support multiple firmware levels if demand exists
* currently can update presets on a QuNeo that is connected to any software under linux.
* very alpha.
* qn_update --help for useage instructions.

## useage examples
### update all presets with QuNeo.json from KMI editor
qn_update -i /path/to/QuNeo.json -n0:15 -c0 -x

### update third preset with preset exported from KMI editor
./qn_update -i exported.quneopreset  -n2 -c2 -x

# Official KMI QuNeo Preset Editor for Linux development started
The qnTools team has joined forces with the KMI team to bring the official KMI QuNeo Preset Editor to Linux and Open Source! See [the announcment](http://forum.keithmcmillen.com/viewtopic.php?f=22&t=1192) for more information.

   