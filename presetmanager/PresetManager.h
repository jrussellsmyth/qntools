/* 
 * File:   PresetManager.h
 * Author: jrussell
 *
 * Created on January 6, 2013, 10:53 PM
 */

#ifndef PRESETMANAGER_H
#define	PRESETMANAGER_H

class PresetManager {
public:
    PresetManager();
    PresetManager(const PresetManager& orig);
    virtual ~PresetManager();
    int get_one();
private:

};

#endif	/* PRESETMANAGER_H */

